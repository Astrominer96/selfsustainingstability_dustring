//<FLAGS>
//#define __GPU
//#define __NOPROTO
//<\FLAGS>

//<INCLUDES>
#include "fargo3d.h"
//<\INCLUDES>

void mon_vazim_vertical_ghost_cpu () {

//<USER_DEFINED>
  INPUT(Density);
  OUTPUT(Slope);
//<\USER_DEFINED>


//<EXTERNAL>
  real* dens = Density->field_cpu;
  real* interm = Slope->field_cpu;
  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = Nx+2*NGHX;
  int size_y = Ny+2*NGHY;
  int size_z = Nz+2*NGHZ;
//<\EXTERNAL>

//<INTERNAL>
  int i;
  int j;
  int k;
  int ll;
  int pp;
  int mm; 
  int temp;
//<\INTERNAL>

//<CONSTANT>
// real Syk(Nz+2*NGHZ);
// real InvVj(Ny+2*NGHY);
//<\CONSTANT>

//<MAIN_LOOP to set every entry to zero>

  i = j = k = 0;
  
#ifdef Z
	for (k=0; k<size_z; k++) {
#endif
#ifdef Y
    for (j=0; j<size_y; j++) {
#endif
#ifdef X
      for (i=0; i<size_x; i++ ) {
#endif

//<#>
				ll = l;
				interm[ll] = 0.0;
//<\#>

#ifdef X
      }
#endif
#ifdef Y
    }
#endif
#ifdef Z
  }
#endif
//<\MAIN_LOOP to set every entry to zero>


//<MAIN_LOOP to pass the ghost values to the active domain>

	i = j = k = 0;

#ifdef Z
	for (k=0; k<size_z; k++) {
#endif
#ifdef Y
    for (j=0; j<size_y; j++) {
#endif
#ifdef X
      for (i=0; i<size_x; i++ ) {
#endif

//<#>
				ll = l;
				
				if(k < NGHZ) {
					temp = k;
					k = k+3;
					
					pp = l;
					//pp = j + (k+3)*Stride_cpu;
					interm[pp] = dens[ll];
					//printf("Zmed(k=2)=%f, Zmed(k=1)=%f, Zmed(k=0)=%f, Zmed(NGHZ)=%f,Zmed(NGHZ+1)=%f,Zmed(NGHZ+2)=%f\n",Zmed(2),Zmed(1),Zmed(0),Zmed(NGHZ),Zmed(NGHZ+1),Zmed(NGHZ+2));
					k = temp;
				}
				
				if(k > Nz+NGHZ-1) {
					temp = k;
					k = k-3;
					mm = l;
					//mm = j + (k-3)*Stride_cpu;
					interm[mm] = dens[ll];
					//printf("Zmed(Nz+NGHZ-3)=%f, Zmed(Nz+NGHZ-2)=%f, Zmed(Nz+NGHZ-1)=%f, Zmed(Nz+NGHZ)=%f,Zmed(Nz+NGHZ+1)=%f,Zmed(Nz+NGHZ+2)=%f\n",Zmed(Nz+NGHZ-3),Zmed(Nz+NGHZ-2),Zmed(Nz+NGHZ-1),Zmed(Nz+NGHZ),Zmed(Nz+NGHZ+1),Zmed(Nz+NGHZ+2));
					k = temp;
				}	
//<\#>

#ifdef X
      }
#endif
#ifdef Y
    }
#endif
#ifdef Z
  }
#endif
//<\MAIN_LOOP to pass the ghost values to the active domain>>
}

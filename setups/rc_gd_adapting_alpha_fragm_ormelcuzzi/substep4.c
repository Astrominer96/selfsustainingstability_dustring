//<FLAGS>
//#define __GPU
//#define __NOPROTO
//<\FLAGS>

//<INCLUDES>
#include "fargo3d.h"
//<\INCLUDES>

// Function copied from Cornelis Dullemond
real finv_approx_ormelcuzzi(real f) {
  real f0 = -0.53916;
  return (0.77277 + 0.33 * atan((f-f0)*20) + 0.62946 * (f-f0))/1.0189 ;
}

// Function copied from Cornelis Dullemond
real OrmelCuzzi_cpu(real a_grain1, real a_grain2, real rho_gas, real R, real alpha) {
  real mu    = 2.3;
  real pi    = 3.14159265359; 
  real sig   = 5.476e-17*4.e0*pi*pow(R0_CGS,-2.0);  // must be dimensionless as well
  real mp    = (1.6726e-24)/MSTAR_CGS;               // 
  real nH2, lamH2, v_therm, nu, Re, vg, yl1, yl2, ystar1, ystar2, t1star, t2star, t12star;
  real soundspeed, H, vturb0, St12star, deltav1_squared, deltav2_squared, Omega_K, relative_v_oc;
  real ts1, ts2, St1, St2, v_rescale;

  // compute the keplerian angular frequency, the gas sound speed
  Omega_K    = sqrt(G*MSTAR/R/R/R);
  H          = ASPECTRATIO*R0*pow(R/R0,FLARINGINDEX+1.0);
  soundspeed = H*Omega_K; 

  // compute thermal properties of the gas
  nH2     = rho_gas/mu/mp;    // number density of molecules
  lamH2   = 1.0/(sig*nH2);    // mean free path of gas molecules
  v_therm = sqrt(8.0/pi)*soundspeed;  // thermal velocity of gas
  nu      = v_therm*lamH2;

  // compute both stopping times (for particle specie 1 and specie 2) as well as the corresponding stokes numbers
  ts1 = (RHODUST/rho_gas)*(a_grain1/v_therm);
  St1 = ts1*Omega_K;
  ts2 = (RHODUST/rho_gas)*(a_grain2/v_therm);
  St2 = ts2*Omega_K;

  // compute turbulent gas velocity as well as St12star
  vturb0 = soundspeed*sqrt(alpha);
  Re     = alpha*soundspeed*soundspeed/(nu*Omega_K); 
  vg     = sqrt(3.0/2.0)*vturb0;
  yl1    = 1/St1;
  yl2    = 1/St2;
  ystar1 = finv_approx_ormelcuzzi(-1/(1+yl1));
  ystar2 = finv_approx_ormelcuzzi(-1/(1+yl2));
  t1star = ystar1*ts1;
  t2star = ystar2*ts2;

  t12star  = fmax(t1star, t2star);
  St12star = t12star*Omega_K;

  v_rescale = sqrt(G_CGS*MSTAR_CGS*pow(R0_CGS,-1.0));

  deltav1_squared = vg*vg*((St1-St2)/(St1+St2))*( St1*St1*pow(St12star+St1,-1.0) - St1*St1*pow(1.0+St1,-1.0) - St2*St2*pow(St12star+St2,-1.0) + St2*St2*pow(1.0+St2,-1.0));
  deltav2_squared = vg*vg*( 2.0*(St12star-pow(Re,-0.5)) + St1*St1*pow(St1+St12star,-1.0) - St1*St1*pow(St1+pow(Re,-0.5),-1.0) + St2*St2*pow(St2+St12star,-1.0) - St2*St2*pow(St2+pow(Re,-0.5),-1.0));
  relative_v_oc = sqrt(deltav2_squared + deltav1_squared);

  return relative_v_oc;
}

void SubStep4_cpu (real dt) {

//<USER_DEFINED>
  INPUT(Density);
  INPUT(Alpha_Parameter);
  OUTPUT(Density);
//<\USER_DEFINED>

//<EXTERNAL>
  real* rho = Density->field_cpu;
  real* rho_gas       = Fluids[0]->Density->field_cpu;
  real* rho_dustsmall = Fluids[1]->Density->field_cpu;
  real* rho_dustlarge = Fluids[2]->Density->field_cpu;
  real* alpha_param   = Alpha_Parameter->field_cpu;
  real r, R, omega, delta_V22;
  real taufrag = TAUFRAG;
  real test;

  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = XIP; 
  int size_y = Ny+2*NGHY-1;
  int size_z = Nz+2*NGHZ-1;
//<\EXTERNAL>

//<INTERNAL>
  int i; //Variables reserved
  int j; //for the topology
  int k; //of the kernels
  int ll;


//<\INTERNAL>
  
//<CONSTANT>
// real GAMMA(1);
// real Sxj(Ny+2*NGHY);
// real Syj(Ny+2*NGHY);
// real Szj(Ny+2*NGHY);
// real Sxk(Nz+2*NGHZ);
// real Syk(Nz+2*NGHZ);
// real Szk(Nz+2*NGHZ);
// real InvVj(Ny+2*NGHY);
//<\CONSTANT>

//<MAIN_LOOP>
  
  i = j = k = 0;
  omega  = sqrt(G*MSTAR/R0/R0/R0);   // angular frequency in the disk midplane
  
#ifdef Z
  for(k=0; k<size_z; k++) {
#endif
#ifdef Y
    for(j=0; j<size_y; j++) {
#endif
#ifdef X
      for(i=0; i<size_x; i++) {
#endif
//<#>

	ll = l;

  r      = Ymed(j);               // spherical radius
  R      = r*sin(Zmed(k));        // cylindrical radius
  omega  = sqrt(G*MSTAR/R/R/R);   // local angular frequency in the disk midplane 

  // Both dust fluids are treated differently in this function (small dust gains density, large dust looses density)
  // Hence we need to identify the fluid that is going to be changed.

#ifdef LINEARSOURCETERM
  if (rho[ll] == rho_dustsmall[ll]) {
    rho[ll] += rho_dustlarge[ll] - rho_dustlarge[ll]*exp(-dt*omega/taufrag);
  }

  if (rho[ll] == rho_dustlarge[ll]) {
    rho[ll] *= exp(-dt*omega/taufrag);
  }
#endif

#if defined (QUADRATICSOURCETERM) && !defined (ORMELCUZZI)
	
  if (rho[ll] == rho_dustsmall[ll]) {
    rho[ll] += rho_dustlarge[ll] - pow(pow(rho_dustlarge[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*MEANRELATIVEVELOCITY*INVGRAINSIZE3*dt/RHODUST,-1.0);
  }

  if (rho[ll] == rho_dustlarge[ll]) {
    rho[ll] = pow(pow(rho[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*MEANRELATIVEVELOCITY*INVGRAINSIZE3*dt/RHODUST,-1.0);
  }
#endif

#if defined (QUADRATICSOURCETERM) && defined (ORMELCUZZI)

  //compute the relative velocity between grains of the same specie
  test = OrmelCuzzi_cpu(1.0/INVGRAINSIZE2, 1.0/INVGRAINSIZE2, rho_gas[ll], R, alpha_param[ll]);

  
  if (test <= MINDELTAV) {
    delta_V22 = 0.0;
  //  //printf("%f \n", rho_dustlarge[ll] - pow(pow(rho_dustlarge[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*delta_V22*INVGRAINSIZE3*dt/RHODUST,-1.0));
  }
  else {
    delta_V22 = test;
  }
  
  if (rho[ll] == rho_dustsmall[ll]) {
    rho[ll] += rho_dustlarge[ll] - pow(pow(rho_dustlarge[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*delta_V22*INVGRAINSIZE2*dt/RHODUST,-1.0);
  }

  if (rho[ll] == rho_dustlarge[ll]) {
    rho[ll] = pow(pow(rho[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*delta_V22*INVGRAINSIZE2*dt/RHODUST,-1.0);
  }
#endif


//<\#>
#ifdef X
      }
#endif  
#ifdef Y
    }
#endif
#ifdef Z
  }
#endif
//<\MAIN_LOOP>
}

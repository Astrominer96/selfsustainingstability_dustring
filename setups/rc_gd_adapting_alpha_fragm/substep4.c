//<FLAGS>
//#define __GPU
//#define __NOPROTO
//<\FLAGS>

//<INCLUDES>
#include "fargo3d.h"
//<\INCLUDES>

void SubStep4_cpu (real dt) {

//<USER_DEFINED>
  INPUT(Density);
  OUTPUT(Density);
//<\USER_DEFINED>

//<EXTERNAL>
  real* rho = Density->field_cpu;
  real* rho_dustsmall = Fluids[1]->Density->field_cpu;
  real* rho_dustlarge = Fluids[2]->Density->field_cpu;
  real r, R, omega;
  real taufrag = TAUFRAG;

  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = XIP; 
  int size_y = Ny+2*NGHY-1;
  int size_z = Nz+2*NGHZ-1;
//<\EXTERNAL>

//<INTERNAL>
  int i; //Variables reserved
  int j; //for the topology
  int k; //of the kernels
  int ll;


//<\INTERNAL>
  
//<CONSTANT>
// real GAMMA(1);
// real Sxj(Ny+2*NGHY);
// real Syj(Ny+2*NGHY);
// real Szj(Ny+2*NGHY);
// real Sxk(Nz+2*NGHZ);
// real Syk(Nz+2*NGHZ);
// real Szk(Nz+2*NGHZ);
// real InvVj(Ny+2*NGHY);
//<\CONSTANT>

//<MAIN_LOOP>
  
  i = j = k = 0;
  omega  = sqrt(G*MSTAR/R0/R0/R0);   // angular frequency in the disk midplane
  
#ifdef Z
  for(k=0; k<size_z; k++) {
#endif
#ifdef Y
    for(j=0; j<size_y; j++) {
#endif
#ifdef X
      for(i=0; i<size_x; i++) {
#endif
//<#>

	ll = l;

  r      = Ymed(j);               // spherical radius
  R      = r*sin(Zmed(k));        // cylindrical radius
  omega  = sqrt(G*MSTAR/R/R/R);   // local angular frequency in the disk midplane 

  // Both dust fluids are treated differently in this function (small dust gains density, large dust looses density)
  // Hence we need to identify the fluid that is going to be changed.

#ifdef LINEARSOURCETERM
  if (rho[ll] == rho_dustsmall[ll]) {
    rho[ll] += rho_dustlarge[ll] - rho_dustlarge[ll]*exp(-dt*omega/taufrag);
  }

  if (rho[ll] == rho_dustlarge[ll]) {
    rho[ll] *= exp(-dt*omega/taufrag);
  }
#endif

#ifdef QUADRATICSOURCETERM
  if (rho[ll] == rho_dustsmall[ll]) {
    rho[ll] += rho_dustlarge[ll] - pow(pow(rho_dustlarge[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*MEANRELATIVEVELOCITY*INVGRAINSIZE2*dt/RHODUST,-1.0);
  }

  if (rho[ll] == rho_dustlarge[ll]) {
    rho[ll] = pow(pow(rho[ll],-1.0) + 6.0*sqrt(G*R0*R0*R0/MSTAR)*FRAGEFFICIENCY*MEANRELATIVEVELOCITY*INVGRAINSIZE2*dt/RHODUST,-1.0);
  }
#endif


//<\#>
#ifdef X
      }
#endif  
#ifdef Y
    }
#endif
#ifdef Z
  }
#endif
//<\MAIN_LOOP>
}

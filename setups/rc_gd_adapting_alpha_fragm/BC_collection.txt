# To define the boundaries it is possible to use any variable or pure
# macro defined in the code. For example, it's allowed to use the
# geometrical variables/macros xmin/xmed(i), ymin/ymed(i),
# zmin/zmax(i), and macros like zone_size_x/y/z. However, it is not
# possible to use macros that are expanded into new macros, like
# Surf_x/y/z nor InvVol, Vol...

# It is also possible to use any variable defined in your SETUP.par.
# All the global variables must be embedded with "'",
# e.g. |...'global_variable'...|, with "'"!

# Example:

# SYMMETRIC:
# Centered: |'OMEGAFRAME'**2+2*'FLARINGINDEX'+'MSTAR'+xmed(i)-ymed(j)+active|active|

# The boundary-related files are case-insensitive, so you can also
# define the following:

# Centered: |'omegaframe'**2+2*'flaring'+'mstar'+xmed(i)-ymed(j)+active|active|

# Be sure that the word "active" is not inside your variables.

# Example of a wrong implementation: |'omegactive'|active|

# Explanation: the parser first substitutes 'omegactive' --> 'omegavariable',
# and then uses 'omegavariable' when applying the boundaries.
# "varible" is a variable defined in the .bound file.


################################################################################
### BELOW: STANDARD BOUNDARIES
################################################################################

SYMMETRIC:
	   Centered:    |a|a|
	   Staggered:   |a|a|a|

ANTISYMMETRIC:
	   Staggered:	|-a|0|a|

NOBOUNDARY:
	    Centered:	|a|a|
	    Staggered:	|a|a|a|

	    
	    
##############################################################
### BELOW: self developed zero torque boundary condition
##############################################################


ZEROTORQUEDENS: #zero torque boundary condition as given in Armitage, Kley eq. 1.81
	Centered: |('Maccretion'/(3.0*'Xmax'*'Nu'))*(1.0-sqrt('Rstar'/Ymed(jgh)))|active| 


##############################################################
### BELOW: self developed BC for radial_colatitude setup without external pressure maximum using EXTRAPOLATION
##############################################################


DENSITY_RAD_COL_RADIAL:
		Centered: |active*pow(Ymed(jgh)/Ymed(jact),-'FLARINGINDEX'-'SIGMASLOPE'-1.0)*exp(-0.5*pow(cos(Zmed(k))/('ASPECTRATIO'*'R0'),2)*pow(sin(Zmed(k))/'R0',-2.0*'FLARINGINDEX'-2.0)*(pow(Ymed(jgh),-2.0*'FLARINGINDEX) - pow(Ymed(jact),-2.0*'FLARINGINDEX')))|active|

DENSITY_RAD_COL_VERTICAL:
		Centered: |active*pow(sin(Zmed(kgh))/sin(Zmed(kact)),-'FLARINGINDEX'-'SIGMASLOPE'-1.0)*exp(-0.5*(pow(Ymed(j),-2.0*'FLARINGINDEX')*'R0'*'R0')*pow('ASPECTRATIO',-2.0)*(pow(cos(Zmed(kgh)),2)*pow(sin(Zmed(kgh)),-2.0*'FLARINGINDEX'-2.0) - pow(cos(Zmed(kact)),2)*pow(sin(Zmed(kact)),-2.0*'FLARINGINDEX'-2.0))))active|

AZIM_VELO_RADIAL:
		Centered: |(vphi + Ymed(jact)*sin(Zmed(k))*'OmegaFrame')*sqrt(Ymed(jact)/Ymed(jgh)) + sqrt('G'*'MSTAR'/sin(Zmed(k))/Ymed(jgh))*0.5*pow('ASPECTRATIO',2.0)*pow(sin(Zmed(k))/'R0',2.0*'FLARINGINDEX')*(2.0+'SIGMASLOPE'-'FLARINGINDEX')*(pow(Ymed(jact),2.0*'FLARINGINDEX')-pow(Ymed(jgh),2.0*'FLARINGINDEX')) -Ymed(jgh)*sin(Zmed(k))*'OmegaFrame'|vphi|

AZIM_VELO_VERTICAL:
		Centered: |(vphi + Ymed(j)*sin(Zmed(kact))*'OmegaFrame')*sqrt(Ymed(jact)/Ymed(jgh))*sqrt(sin(Zmed(kact))/sin(Zmed(kgh)))+ sqrt('G'*'MSTAR'/sin(Zmed(kgh))/Ymed(j))*(0.5*pow('ASPECTRATIO',2.0)*pow(Ymed(j)/'R0',2.0*'FLARINGINDEX')*(2.0+'SIGMASLOPE'-'FLARINGINDEX')*(pow(sin(Zmed(kact)),2.0*'FLARINGINDEX')-pow(sin(Zmed(kgh)),2.0*'FLARINGINDEX')) + 0.25*(1.0-2.0*'FLARINGINDEX')*(pow(cos(Zmed(kact))/sin(Zmed(kact)),2)-pow(cos(Zmed(kgh))/sin(Zmed(kgh)),2)))-Ymed(j)*sin(Zmed(kgh)))*'OmegaFrame'|vphi|


##############################################################
### BELOW: self developed BC for gas radial_colatitude setup plus external pressure maximum
### the boundary condition for the azimuthal velocity as well as for the density in colatitude direction 
### is calculated by doing an extrapolation using the first derivative
##############################################################


DENSITY_RAD_COL_RADIAL:
		Centered: |active*pow(Ymed(jgh)/Ymed(jact),-'FLARINGINDEX'-'SIGMASLOPE'-1.0)*exp(-0.5*pow(cos(Zmed(k))/('ASPECTRATIO'*'R0'),2)*pow(sin(Zmed(k))/'R0',-2.0*'FLARINGINDEX'-2.0)*(pow(Ymed(jgh),-2.0*'FLARINGINDEX) - pow(Ymed(jact),-2.0*'FLARINGINDEX')))|active|

DENSITY_RAD_COL_VERTICAL_TOP:
		Centered: |density[i+j*Nx+(NGHZ+1)*Stride] + (density[i+j*Nx+(NGHZ)*Stride] - density[i+j*Nx+(NGHZ+2)*Stride])/(Zmed(NGHZ) - Zmed(NGHZ+2))*(Zmed(kgh) - Zmed(NGHZ+1))|active|

DENSITY_RAD_COL_VERTICAL_BOTTOM:
		Centered: |density[i+j*Nx+(NGHZ+Nz-2)*Stride] + (density[i+j*Nx+(NGHZ+Nz-1)*Stride] - density[i+j*Nx+(NGHZ+Nz-3)*Stride])/(Zmed(NGHZ+Nz-1) - Zmed(NGHZ+Nz-3))*(Zmed(kgh) - Zmed(NGHZ+Nz-2))|active|

AZIM_VELO_RAD_COL_RADIAL:
		Centered: |(vphi + Ymed(jact)*sin(Zmed(k))*'OmegaFrame')*sqrt(Ymed(jact)/Ymed(jgh)) + sqrt('G'*'MSTAR'/sin(Zmed(k))/Ymed(jgh))*0.5*pow('ASPECTRATIO',2.0)*pow(sin(Zmed(k))/'R0',2.0*'FLARINGINDEX')*(2.0+'SIGMASLOPE'-'FLARINGINDEX')*(pow(Ymed(jact),2.0*'FLARINGINDEX')-pow(Ymed(jgh),2.0*'FLARINGINDEX')) -Ymed(jgh)*sin(Zmed(k))*'OmegaFrame'|vphi|

AZIM_VELO_RAD_COL_VERTICAL_BOTTOM:
		Centered: |(vx[i+j*Nx+(NGHZ+Nz-2)*Stride] + Ymed(j)*sin(Zmed(NGHZ+Nz-2))*'OmegaFrame') + (vx[i+j*Nx+(NGHZ+Nz-1)*Stride] - vx[i+j*Nx+(NGHZ+Nz-3)*Stride] + Ymed(j)*'OmegaFrame'*(sin(Zmed(NGHZ+Nz-1))-sin(Zmed(NGHZ+Nz-3))))/(Zmed(NGHZ+Nz-1) - Zmed(NGHZ+Nz-3))*(Zmed(kgh) - Zmed(NGHZ+Nz-2)) - Ymed(j)*sin(Zmed(kgh))*'OmegaFrame' |vphi|

AZIM_VELO_RAD_COL_VERTICAL_TOP:
		Centered: |(vx[i+j*Nx+(NGHZ+1)*Stride] + Ymed(j)*sin(Zmed(NGHZ+1))*'OmegaFrame') + (vx[i+j*Nx+(NGHZ)*Stride] - vx[i+j*Nx+(NGHZ+2)*Stride] + Ymed(j)*'OmegaFrame'*(sin(Zmed(NGHZ))-sin(Zmed(NGHZ+2))))/(Zmed(NGHZ) - Zmed(NGHZ+2))*(Zmed(kgh) - Zmed(NGHZ+1)) - Ymed(j)*sin(Zmed(kgh))*'OmegaFrame' |vphi|



##############################################################
### BELOW: self developed BC for dust in radial_colatitude_gas_dust setup
### BC for the density in colatitude direction is the same as for gas
### (DENSITY_RAD_COL_VERTICAL_BOTTOM, DENSITY_RAD_COL_VERTICAL_TOP)
### The BC for dust-density in radial direction can also be copied from the 
### radial_colatitude setup above (DENSITY_RAD_COL_RADIAL).
### Left is only the BC for azimuthal velocity in radial and colatitude direction
##############################################################

AZIM_VELO_RAD_COL_GD_RADIAL:
		Centered: |(vphi + Ymed(jact)*sin(Zmed(k))*'OmegaFrame')*sqrt(Ymed(jact)/Ymed(jgh))*sin(Zmed(k)) - Ymed(jgh)*sin(Zmed(k))*'OmegaFrame'|vphi|

AZIM_VELO_RAD_COL_GD_VERTICAL:
		Centered: |(vphi + Ymed(jact)*sin(Zmed(k))*'OmegaFrame')*sin(Zmed(kgh))/sin(Zmed(kact)) - Ymed(jgh)*sin(Zmed(k))*'OmegaFrame'|vphi|

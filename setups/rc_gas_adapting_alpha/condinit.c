#include "fargo3d.h"

// Funktion berechnet die dichte des Gasis in der Midplane auf Grund eines Powerlaws
real rho_midplane_powerlaw(real cyl_radius, real prefac) {

  real rho_midplane_pw;
  rho_midplane_pw = prefac*pow(cyl_radius/R0,-FLARINGINDEX-SIGMASLOPE-1.0);
  return rho_midplane_pw;
}

// Funktion berechnet die dichte des Gasis in der Midplane auf Grund einer Gaußverteilung
real rho_midplane_external(real cyl_radius, real prefac) {
    
  real rho_midplane_max;
  rho_midplane_max = prefac*exp(-0.5*pow(cyl_radius-LOCPRESSUREMAX*R0,2.0)*pow(R0*WIDTHPRESSUREMAX,-2.0));
  return rho_midplane_max;
}

// Funktion berechnet die dichte des Gasis bei beliebigem Z. Dafür braucht es die bereits berechnete Verteilung in der Midplane
real rho_disk(real z, real H, real rho_midplane) {

  real rho_entire_disk;
  rho_entire_disk = rho_midplane*exp(-0.5*pow(z/H,2.0));
  return rho_entire_disk;
}

real CSpeed(real cyl_radius, real omega) {
    
  real cs;
  cs = ASPECTRATIO*R0*pow(cyl_radius/R0,FLARINGINDEX+1.0)*omega;
  return cs;
}


#if defined (ALPHAVISCOSITY) && defined (DYNAMICALPHAVISCOSITY)

//define function which calculates angular frequency of gas in the midplane aka Omega_prime
real ang_freq_gas(real R, real H, real rho_0_pw, real rho_0_max, real rho_midplane, real Omega_k) {
  real beta  = 1.0-2.0*FLARINGINDEX;
  real epsilon = beta + 3.0 + 2.0*SIGMASLOPE;
  real ang_freq_squared_1 = (rho_0_pw/rho_midplane)*epsilon + 2.0*(rho_0_max/rho_midplane)*beta + 2.0*(rho_0_max/rho_midplane)*R*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0);
  real ang_freq_squared_2 = pow(Omega_k, 2.0) - 0.5*pow(Omega_k, 2.0)*pow(H/R,2.0)*ang_freq_squared_1;
  return sqrt(ang_freq_squared_2);
}

//define function which calculates the derivative of rho_0_pw/rho_midplane
real derivative_rho_pw_rho_midplane(real R, real rho_0_pw, real rho_0_max, real rho_midplane) {
  real comp1 = (-FLARINGINDEX - 1.0 -SIGMASLOPE)*(rho_0_pw/rho_midplane)*(1/R0)*pow(R/R0, -1.0);
  real comp2 = -pow(rho_0_pw/rho_midplane,2.0)*(1/R0)*(-FLARINGINDEX -1.0 -SIGMASLOPE)*pow(R/R0,-1.0);
  real comp3 = +rho_0_pw*rho_0_max*pow(rho_midplane,-2.0)*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0);
  return comp1+comp2+comp3;
}

//define function which calculates the derivative of rho_0_max/rho_midplane
real derivative_rho_max_rho_midplane(real R, real rho_0_pw, real rho_0_max, real rho_midplane) {
  real comp1 = -(rho_0_max/rho_midplane)*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0);
  real comp2 = -rho_0_max*rho_0_pw*pow(rho_midplane,-2.0)*(-FLARINGINDEX -1.0 -SIGMASLOPE)*pow(R/R0,-1.0)*(1/R0);
  real comp3 = rho_0_max*rho_0_max*pow(rho_midplane,-2.0)*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0);
  return comp1+comp2+comp3;
}

//define function which calculates the first derivative of angular frequency of the gas in the midplane
real derivative_ang_freq_gas(real R, real H, real rho_0_pw, real rho_0_max, real rho_midplane, real Omega_k) {
  real beta  = 1.0-2.0*FLARINGINDEX;
  real epsilon = beta + 3.0 + 2.0*SIGMASLOPE;
  real ang_freq_prime = ang_freq_gas(R, H, rho_0_pw, rho_0_max, rho_midplane, Omega_k);

  real comp1 = -3.0*pow(Omega_k,2.0)/R;
  real comp2 = -0.5*G*MSTAR*pow(ASPECTRATIO,2.0)*pow(R0,-2.0*FLARINGINDEX)*(2.0*FLARINGINDEX-3.0)*pow(R,2.0*FLARINGINDEX-4.0);
  real comp3 = (rho_0_pw/rho_midplane)*epsilon + 2.0*(rho_0_max/rho_midplane)*beta + 2.0*(rho_0_max/rho_midplane)*R*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0); 
  real comp4 = -0.5*pow(Omega_k,2.0)*pow(H/R,2.0)*derivative_rho_pw_rho_midplane(R, rho_0_pw, rho_0_max, rho_midplane)*epsilon;
  real comp5 = -pow(Omega_k,2.0)*pow(H/R,2.0)*derivative_rho_max_rho_midplane(R, rho_0_pw, rho_0_max, rho_midplane)*beta;
  real comp6 = -pow(Omega_k,2.0)*pow(H/R,2.0)*derivative_rho_max_rho_midplane(R, rho_0_pw, rho_0_max, rho_midplane)*R*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0);
  real comp7 = -pow(Omega_k,2.0)*pow(H/R,2.0)*(rho_0_max/rho_midplane)*( (R-LOCPRESSUREMAX*R0)*pow(R0*WIDTHPRESSUREMAX,-2.0) + R*pow(R0*WIDTHPRESSUREMAX,-2.0) );
  return 0.5*pow(ang_freq_prime,-1.0)*(comp1 + comp2*comp3 + comp4 + comp5 + comp6 + comp7);
}

//define function which calculates the alpha parameter as function of cylindrical radius if \Sigma*\nu = const (\Omega_gas = \Omega_K)
real alpha_par_Sigma_nu_const(real R) {

  real comp1 = pow(R/R0,-SIGMASLOPE+2.0*FLARINGINDEX+0.5);
  real comp2 = EXTERNALPRESSUREMAX*pow(LOCPRESSUREMAX,-SIGMASLOPE)*pow(R/R0,2.0*FLARINGINDEX+0.5)*exp(-0.5*pow(R-LOCPRESSUREMAX*R0,2.0)*pow(R0*WIDTHPRESSUREMAX,-2.0));
  return ALPHA*pow(comp1 + comp2,-1.0);
}

//define function which calculates the alpha parameter as function of cylindrical radius if \Omega_gas given by contributions of gravity and pressure grad.
real alpha_par_Sigma_nu_variable(real R, real Omega_prime, real derivative_Omega_prime) {

  real comp1 = -1.5*ALPHA*(Omega_prime/R)*pow(derivative_Omega_prime,-1.0);
  real comp2 = pow(R/R0,-SIGMASLOPE+2.0*FLARINGINDEX+0.5);
  real comp3 = EXTERNALPRESSUREMAX*pow(LOCPRESSUREMAX,-SIGMASLOPE)*pow(R/R0,2.0*FLARINGINDEX+0.5)*exp(-0.5*pow(R-LOCPRESSUREMAX*R0,2.0)*pow(R0*WIDTHPRESSUREMAX,-2.0));
  return comp1*pow(comp2 + comp3,-1.0);
}
#endif


void Init() {

#if defined (ALPHAVISCOSITY) && defined (DYNAMICALPHAVISCOSITY)
  OUTPUT(Alpha_Parameter);
#endif  

  OUTPUT(Density);
  OUTPUT(Energy);
  OUTPUT(Vx);
  OUTPUT(Vy);
  OUTPUT(Vz);

  int i,j,k;
  real r, R, omega, height, beta = 1.0-2.0*FLARINGINDEX; 
  real rho_midplane, rho_0_pw, rho_0_max, H;
  real soundspeed, vphi_squared, gravitational, pressure_grad;
  real pi = 3.14159265359;
  real rho_0_pw_prefac  = (1.0/sqrt(2.0*pi))*(SIGMA0/ASPECTRATIO/R0);
  real rho_0_max_prefac = (EXTERNALPRESSUREMAX/sqrt(2.0*pi))*(SIGMA0/ASPECTRATIO/R0)*pow(LOCPRESSUREMAX,-FLARINGINDEX-SIGMASLOPE-1);
  
  real *vphi = Vx->field_cpu;
  real *vr   = Vy->field_cpu;
  real *vz   = Vz->field_cpu;
  real *rho  = Density->field_cpu;
  real *cs   = Energy->field_cpu;
  real *alpha_parameter  = Alpha_Parameter->field_cpu;

  i = j = k = 0;
  
  for (k = 0; k < Nz+2*NGHZ; k++) {  
    for (j = 0; j < Ny+2*NGHY; j++) {
      
      r      = Ymed(j);          // spherical radius
      R      = r*sin(Zmed(k));   // cylindrical radius
      height = cos(Zmed(k))*r;
      omega  = sqrt(G*MSTAR/R/R/R);   // angular frequency in the disk midplane
      H      = ASPECTRATIO*R0*pow(R/R0,FLARINGINDEX+1.0);
      
      rho_0_pw     = rho_midplane_powerlaw(R, rho_0_pw_prefac); //Midplane density
      rho_0_max    = rho_midplane_external(R, rho_0_max_prefac); //Midplane density
      rho_midplane = rho_0_pw + rho_0_max;
      soundspeed   = CSpeed(R, omega);


      if (Fluidtype == GAS) {
  //----------------------------------------------------------------gas properties-----------------------------------------------------------------
  // the following line can be used as initial condition IF the disk does not include an external pressure maximum
        //vphi_squared = G*MSTAR*R*R*pow(r,-3.0) - 0.5*(G*MSTAR/R)*pow(H/R,2.0)*(pow(height/H,2.0)*(beta-3) + (beta+3.0+2.0*SIGMASLOPE))
        //vphi[l] = sqrt(vphi_squared);

  // the following code line can be used as inital condition if the disk has an external pressure maximum and the azimuthal velocity balances pressure gradient-force and gravity
  // the expression for vphi can be calculated by using the expression above for the density rho(r_cyl, z) and the expression for the sound speed^2 as well as the radial component of 
  // the navier stokes equation.
        gravitational = G*MSTAR*pow(r,-3.0)*R*R;
        pressure_grad = - 0.5*(G*MSTAR/R)*pow(H/R,2.0)*( pow(height/H,2.0)*(beta-3.0) + (beta+3.0+2.0*SIGMASLOPE)*(rho_0_pw/rho_midplane) + 2.0*(rho_0_max/rho_midplane)*beta + 2.0*(rho_0_max/rho_midplane)*R*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0));
        vphi[l] = sqrt(gravitational + pressure_grad) + NOISELVL*soundspeed*2.0*(drand48()-0.5);
        vphi[l] -= OMEGAFRAME*R;

        cs[l]   = soundspeed;
        rho[l]  = rho_disk(height, H, rho_midplane);
        vz[l]    = 0.0 + NOISELVL*soundspeed*2.0*(drand48()-0.5);
        vr[l]    = 0.0 + NOISELVL*soundspeed*2.0*(drand48()-0.5);

        

#if defined (ALPHAVISCOSITY) && defined (DYNAMICALPHAVISCOSITY)
  //----------------------------------------------------------------alpha parameter------------------------------------------------------------------
  // definition of alpha value if \Sigma * \nu = const (if \Omega_gas = \Omega_kepler)
      //alpha_parameter[l] = alpha_par_Sigma_nu_const(R);

  // definition of alpha value if \Omega_gas consists of contribution by gravity and pressure gradient
        real Omega_prime = ang_freq_gas(R, H, rho_0_pw, rho_0_max, rho_midplane, omega);
        real derivative_Omega_prime = derivative_ang_freq_gas(R, H, rho_0_pw, rho_0_max, rho_midplane, omega);
        alpha_parameter[l] = alpha_par_Sigma_nu_variable(R, Omega_prime, derivative_Omega_prime);
  //----------------------------------------------------------------unperturbed gas and dust disk------------------------------------------------------------ 
        //vr[l]   = -1.5*alpha_par_Sigma_nu_variable(R, Omega_prime, derivative_Omega_prime)*omega*H*H/R + NOISELVL*(drand48()-0.5);
#endif


      }
    } 
  }
}


void CondInit() {

  Fluids[0] = CreateFluid("gas",GAS);
  SelectFluid(0);
  Init();
}

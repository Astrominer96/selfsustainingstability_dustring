Setup			rc_gas_adapting_alpha

### Noise parameters
NOISELVL            0.01

### Alpha parameters to check the recipe from Dullemond Penzlin 2018 (eq. 11)
ALPHAVISCCFL        3.0e-3          Alpha parameter close to inner boundary due to alpha recipe of Dullemond Penzlin 2018
ALPHADUSTDIFF       1.0e-2          Alpha parameter that is used in the dust diffusion coefficient source file when oszillation occur.

### Disk gas parameters
AspectRatio         0.05            Thickness over Radius in the disc
Sigma0		    0.017097749640525888	Surface Density at r=1 (entspricht grob 209 g/cm^2 bei r=1)
SigmaSlope		    0.5		        Slope for the surface density
FlaringIndex	    0.0		        Slope for the aspect-ratio
Alpha               1.0e-5
PHIDUST             -1.0            Tuningparameter similar equation of eq.11 in Dullemond, Penzlin 2018
PHIGAS              1.0
Externalpressuremax 4.0             Specifies if there is an external pressure maximum. anything different than 0.0 means yes, 0.0 no
Locpressuremax      2.0             Location of Pressure maximum scalefree but will be multiplied by R0
Widthpressuremax    0.2             Width of the pressure max scalefree but will be multiplied by R0


# Radial range for damping (in period-ratios). Values smaller than one
# prevent damping.

DampingZone     	1.6565

# Characteristic time for damping, in units of the inverse local
# orbital frequency. Higher values means lower damping

TauDamp         	0.6

### Planet parameters

PlanetConfig		planets/zero.cfg
ThicknessSmoothing 	0.6
RocheSmoothing 	    0.0
Eccentricity		0.0
ExcludeHill		    no

IndirectTerm		Yes

### Mesh parameters

# Nx is default mäßig auf 1 gesetzt
Ny                  300		        Radial number of zones 
Nz			    	101		        number of zones for colatitude
Xmin				-3.14159265359	
Xmax				3.14159265359	
Ymin				0.5		        Inner boundary radius
Ymax				10.0		    Outer boundary radius
Zmin			    1.420796	    lower boundary (precicely, the absolute value of zmin, zmax is arctan(AspectRatio), which is, for small arguments rougly AspectRatio
Zmax				1.720796    upper boundary
OmegaFrame     	    0.0		      Angular velocity for the frame of reference (If Frame is F).
Frame				F		        Method for moving the frame of reference
Spacing             log

### Output control parameters

DT			        0.6283185307179586     Physical time between fine-grain outputs (0.6 entspricht 0.1x der orbital time von Jupiter)
Ninterm	 	        500	                    Number of DTs between scalar fields outputs
Ntot		        50000                    Total number of DTs

OutputDir		@outputs/rc_gas_adapting_alpha/am5w2

### Plotting parameters
PlotLog		        yes

### GPU parameters
FuncArchFile        setups/rc_gas_adapting_alpha/func_arch.cfg

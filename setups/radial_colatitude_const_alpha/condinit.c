#include "fargo3d.h"


real rho_midplane_powerlaw(real cyl_radius, real prefac) {

  real rho_midplane_pw;
  rho_midplane_pw = prefac*pow(cyl_radius/R0,-FLARINGINDEX-SIGMASLOPE-1);
  return rho_midplane_pw;
}

// Funktion berechnet die dichte des Gasis in der Midplane auf Grund einer Gaußverteilung
real rho_midplane_external(real cyl_radius, real prefac) {
    
  real rho_midplane_max;
  rho_midplane_max = prefac*exp(-0.5*pow(cyl_radius-LOCPRESSUREMAX*R0,2.0)*pow(R0*WIDTHPRESSUREMAX,-2.0));
  return rho_midplane_max;
}

// Funktion berechnet die dichte des Gasis bei beliebigem Z. Dafür braucht es die bereits berechnete Verteilung in der Midplane
real rho_disk(real z, real H, real rho_midplane) {

  real rho_entire_disk;
  rho_entire_disk = rho_midplane*exp(-0.5*pow(z/H,2.0));
  return rho_entire_disk;
}

real CSpeed(real cyl_radius, real omega) {
    
  real cs;
  cs = ASPECTRATIO*R0*pow(cyl_radius/R0,FLARINGINDEX+1.0)*omega;
  return cs;
}


void Init() {
  
  OUTPUT(Density);
  OUTPUT(Energy);
  OUTPUT(Vx);
  OUTPUT(Vy);
  OUTPUT(Vz);

  int i,j,k;
  real r, R, omega, height, beta, rho_midplane, rho_0_pw, rho_0_max, H;
  real soundspeed, vphi_squared, gravitational, pressure_grad, SurfaceDen_pw, SurfaceDen_max;
  real pi = 3.14159265359;
  real rho_0_pw_prefac  = (1.0/sqrt(2.0*pi))*(SIGMA0/ASPECTRATIO/R0);
  real rho_0_max_prefac = (EXTERNALPRESSUREMAX/sqrt(2.0*pi))*(SIGMA0/ASPECTRATIO/R0)*pow(LOCPRESSUREMAX,-FLARINGINDEX-SIGMASLOPE-1);
  
  real *vphi   = Vx->field_cpu;
  real *vr     = Vy->field_cpu;
  real *vz     = Vz->field_cpu;
  real *rho    = Density->field_cpu;

#ifdef ADIABATIC
  real *e  = Energy->field_cpu;
#endif
#ifdef ISOTHERMAL
  real *cs = Energy->field_cpu;
#endif

  i = j = k = 0;
  
  for (k = 0; k < Nz+2*NGHZ; k++) {  
    for (j = 0; j < Ny+2*NGHY; j++) {
      
      r      = Ymed(j);          // spherical radius
      R      = r*sin(Zmed(k));   // cylindrical radius
      height = cos(Zmed(k))*r;
      omega  = sqrt(G*MSTAR/R/R/R);   // angular frequency in the disk midplane
      beta   = 1.0-2.0*FLARINGINDEX;
      H      = ASPECTRATIO*R0*pow(R/R0,FLARINGINDEX+1.0);
      
      rho_0_pw     = rho_midplane_powerlaw(R, rho_0_pw_prefac);
      rho_0_max    = rho_midplane_external(R, rho_0_max_prefac);
      rho_midplane = rho_0_pw + rho_0_max;
      rho[l]       = rho_disk(height, H, rho_midplane);
      
      soundspeed   = CSpeed(R, omega);

#ifdef ISOTHERMAL 
      cs[l] = soundspeed;
#endif
#ifdef ADIABATIC
      e[l] = pow(soundspeed,2)*rho[l]/(GAMMA-1.0);
#endif
      
  // the following line can be used as initial condition IF the disk does not include an external pressure maximum
      vphi_squared = G*MSTAR*R*R*pow(r,-3.0) - 0.5*(G*MSTAR/R)*pow(H/R,2.0)*(pow(height/H,2.0)*(beta-3.0) + (beta+3.0+2.0*SIGMASLOPE));
      vphi[l] = sqrt(vphi_squared) + NOISELVL*soundspeed*2.0*(drand48()-0.5);
      

  // the following code line can be used as inital condition if the disk has an external pressure maximum and the azimuthal velocity balanced pressure gradient-force and gravity
  // the expression for vphi can be calculated by using the expression above for the density rho(r_cyl, z) and the expression for the sound speed^2 as well as the radial component of 
  // the navier stokes equation.
      //gravitational = G*MSTAR*pow(r,-3.0)*R*R;
      //pressure_grad = - 0.5*(G*MSTAR/R)*pow(H/R,2.0)*( pow(height/H,2.0)*(beta-3.0) + (beta+3+2*SIGMASLOPE)*(rho_0_pw/rho_midplane) + 2*(rho_0_max/rho_midplane)*beta + 2*(rho_0_max/rho_midplane)*R*(R-LOCPRESSUREMAX*R0)/pow(R0*WIDTHPRESSUREMAX,2.0));
      //vphi_squared = gravitational + pressure_grad;
      //vphi[l] = sqrt(vphi_squared);
      
      vphi[l] -= OMEGAFRAME*R;
      vz[l]    = 0.0 + NOISELVL*soundspeed*2.0*(drand48()-0.5);
      vr[l]    = 0.0 + NOISELVL*soundspeed*2.0*(drand48()-0.5);
  } 
}

}

void CondInit() {
   Fluids[0] = CreateFluid("gas",GAS);
   SelectFluid(0);
   Init();
}
//<FLAGS>
//#define __GPU
//#define __NOPROTO
//<\FLAGS>

//<INCLUDES>
#include "fargo3d.h"
//<\INCLUDES>


void AdaptAlpha_cpu() {

//<USER_DEFINED>
  // INPUT(Total_Dust_Density); this field is currently not necessary because only the small grains influence the alpha parameter
  INPUT(Density_Gas_unperturbed);
  INPUT(Density_Dust_unperturbed); // unperturbed dust density of small grains!
  INPUT(Density);
  OUTPUT(Alpha_Parameter);
//<\USER_DEFINED>

//<INTERNAL>
  int i;
  int j;
  int k;
//<\INTERNAL>

//<EXTERNAL>
  //real* rho_dust_total = Total_Dust_Density->field_cpu;
  real* rho_gas0 = Density_Gas_unperturbed->field_cpu;
  real* rho_dustsmall0 = Density_Dust_unperturbed->field_cpu; // unperturbed dust density of small grains!
  real* rho_gas  = Fluids[0]->Density->field_cpu;
  real* rho_dustsmall = Fluids[1]->Density->field_cpu;
  real* alpha_param = Alpha_Parameter->field_cpu;
  real temp;
  int pitch  = Pitch_cpu;
  int stride = Stride_cpu;
  int size_x = Nx;
  int size_y = Ny+2*NGHY;
  int size_z = Nz+2*NGHZ;
  int ll;
//<\EXTERNAL>
 
//<MAIN_LOOP>
  for (k=0; k<size_z; k++) {
    for (j=0; j<size_y; j++) {
      for (i=0; i<size_x; i++) {

//<#>
        ll = l;
        temp = ALPHA*pow(rho_dustsmall[ll]/rho_dustsmall0[ll], PHIDUST)*pow(rho_gas[ll]/rho_gas0[ll], PHIGAS);

        if (temp > 1.0e-2) {
          alpha_param[ll] = 1.0e-2;
        }
        else {
          alpha_param[ll] = temp;
        }
//<\#>
      }
    }
  }
//<\MAIN_LOOP>
}

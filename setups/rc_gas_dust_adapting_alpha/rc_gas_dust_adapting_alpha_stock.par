Setup			rc_gas_dust_adapting_alpha

### Noise parameters
NOISELVL            0.01

### Alpha parameters to check the recipe from Dullemond Penzlin 2018 (eq. 11)
ALPHAVISCCFL        3.0e-3          Alpha parameter close to inner boundary due to alpha recipe of Dullemond Penzlin 2018
ALPHADUSTDIFF       1.0e-2          Alpha parameter that is used in the dust diffusion coefficient source file when oszillation occur.

### Disk gas parameters
AspectRatio         0.05                  Thickness over Radius in the disc
Sigma0	            0.017097749640525888  Surface Density at r=1 (entspricht grob 209 g/cm^2 bei r=1)
SigmaSlope          0.5		  Slope for the surface density
FlaringIndex	    0.0		          Slope for the aspect-ratio
Alpha               1.0e-3
PHIDUST             -1.0            Tuningparameter similar equation of eq.11 in Dullemond, Penzlin 2018
PHIGAS              1.0
Externalpressuremax 4.0             Specifies if there is an external pressure maximum. anything different than 0.0 means yes, 0.0 no
Locpressuremax      2.0             Location of Pressure maximum scalefree but will be multiplied by R0
Widthpressuremax    0.2             Width of the pressure max scalefree but will be multiplied by R0


### Dust parameters for constant Stokes number
Invstokes1          1.6666e5            Inverse of Stokes Number for Dust component 1
Invstokes2          1.6666e3            Inverse of Stokes Number for Dust component 2
Invstokes3          1.6666e1            Inverse of Stokes Number for Dust component 3
Invstokes4          1.66666             Inverse of Stokes Number for Dust component 4

### Dust parameters for constant Grain size and no grain size distribution
Xi                  1.0e-2              Ratio of small grain density to large grain density
Invgrainsize1       1.3297872340425531e+19            Inverse of dim less grainsize corresponds to 1/45micronmeter
Invgrainsize2       1.3297872340425532e+15            corresponds to 1/0.45cm

### Dust parameters for fragementation 
TAUFRAG             1.0e6                 Fragmentation time scale in units of local Omega 

### Dust parameters for constant Grain size and a grain size distribution
AMIN                5.0e-6              minimum grain size of grain size distribution (in cm)
AMAX                1.0                 maximum grain size (in cm), AMIN and AMAX will be made dimensionless in the condinit.c file
p                   -2.5                Power law index for the grain size distribution: n(a) propto pow(a, p) with [n(a)] = [#particles per volume per grainsize])
Epsilon             0.01                Dust to gas ratio

Rhodust             215441612529.2328       Density of dust particles in dimensionless units (corresponds to 2.0 g*cm^-3)


# Radial range for damping (in period-ratios). Values smaller than one
# prevent damping.

DampingZone     1.6565

# Characteristic time for damping, in units of the inverse local
# orbital frequency. Higher values means lower damping

TauDamp         0.6

### Planet parameters

PlanetConfig		planets/zero.cfg
ThicknessSmoothing 	0.6
RocheSmoothing 	    0.0
Eccentricity		0.0
ExcludeHill		    no

IndirectTerm		Yes

### Mesh parameters

# Nx is default mäßig auf 1 gesetzt
Ny              300		        Radial number of zones 250
Nz			    101		        number of zones for colatitude 201
Xmin			-3.14159265359	
Xmax			3.14159265359	
Ymin			0.5		        Inner boundary radius
Ymax			10.0		    Outer boundary radius
Zmin			1.420796	    lower boundary (precicely, the absolute value of zmin, zmax is arctan(AspectRatio), which is, for small arguments rougly AspectRatio
Zmax			1.720796        upper boundary
OmegaFrame     	0.0		        Angular velocity for the frame of reference (If Frame is F).
Frame			F		        Method for moving the frame of reference
Spacing         log

### Output control parameters

DT			0.6283185307179586     Physical time between fine-grain outputs (0.6 entspricht 0.1x der orbital time von Jupiter)
Ninterm	 	500	                    Number of DTs between scalar fields outputs
Ntot		287000                    Total number of DTs

OutputDir		@outputs/rc_gas_dust_adapting_alpha/fout_stdifffeedvaralpha

### Plotting parameters
PlotLog		yes

### GPU parameters
FuncArchFile            setups/rc_gas_dust_adapting_alpha/func_arch.cfg

# You may enter a description between the BEGIN and END labels.
# BEGIN
# This setup is a (R,THETA) setup to simulate a protoplanetary disk.
# Instead of a constant Stokes number within Disks, we use constant grain sizes
# END
#-------------------------------------------------------------------------

FLUIDS := 0 1 2  # this variable defines the index of each fluid. 
NFLUIDS = 3
FARGO_OPT += -DNFLUIDS=${NFLUIDS}
FARGO_OPT += -DFLOOR
FARGO_OPT += -DDRAGFORCE
FARGO_OPT += -DCONSTANTGRAINSIZE
FARGO_OPT += -DDUSTDIFFUSION
FARGO_OPT += -DFEEDBACKDUST2GAS
#FARGO_OPT += -DFRAGMENTATIONSOURCE


#Monitoring options
MONITOR_SCALAR = MASS | MOM_X | TORQ
MONITOR_Y_RAW  = TORQ

# the following argument needs to be activated such that the values of the ghost cells are outputed by the monitoring function
#MONITOR_2D     = VAZIM_VERTICAL_GHOST

#Damping zones in the active mesh
FARGO_OPT += -DSTOCKHOLM

#Dimensions within the simulation
FARGO_OPT +=  -DX
FARGO_OPT +=  -DY
FARGO_OPT +=  -DZ

#Equation of State
FARGO_OPT +=  -DISOTHERMAL

#Coordinate System.
FARGO_OPT +=  -DSPHERICAL

#Legacy files for outputs
FARGO_OPT += -DLEGACY

FARGO_OPT += -DPOTENTIAL

#Calculation of viscosity within the disk
# Checkalpharecipe is meant to not use the dynamic alpharecipe from Dullemond Penzlin but assigns a different global constant alpah value to dust diffusion 
# than to viscous tensor and the cfl criterion. 
FARGO_OPT += -DALPHAVISCOSITY 
FARGO_OPT += -DDYNAMICALPHAVISCOSITY
# FARGO_OPT += -DCHECKALPHARECIPE

#Cuda blocks
ifeq (${GPU}, 1)
FARGO_OPT += -DBLOCK_X=16
FARGO_OPT += -DBLOCK_Y=16
FARGO_OPT += -DBLOCK_Z=1
endif
